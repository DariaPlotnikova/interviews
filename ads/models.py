from typing import Any

from pydantic import BaseModel

from sqlalchemy.ext.declarative import declarative_base

Base: Any = declarative_base()


class CampaignDb(Base):
    __tablename__ = "campaign"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    status = Column(String, nullable=False)
    name = Column(String(length=100), nullable=False)
    account_id = Column(UUID, ForeignKey("account.id", ondelete="CASCADE"), nullable=False)


class AdDb(Base):
    __tablename__ = "ad"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    campaign_id = Column(UUID(as_uuid=True), ForeignKey("campaign.id"), nullable=False)
    account_id = Column(UUID, ForeignKey("account.id", ondelete="CASCADE"), nullable=False)
    status = Column(String, nullable=False)
    name = Column(String, nullable=False)
    target = Column(JSONB)
    site_url = Column(String(length=2048))


class AccountDb(Base):
    __tablename__ = "account"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    name = Column(String)
