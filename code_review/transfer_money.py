# Задача:
#   Пользователь А хочет перевести деньги со своего счета пользователю Б.
#   При успешном переводе у А сообщение "Успешно отправлено", у Б -
#     сообщение "Вам отправлен перевод".


# views.py -------------------------------------------------------------------

class TransferMoney(View):

    def get(self, request, **kwargs):
        s = False
        user = User.get_by_id(self.kwargs["user_1"])
        user_2 = User.get_by_id(self.kwargs["user_2"])
        amount = self.kwargs["amount"]

        if user.amount > amount:
            user.amout -= amount
            user_2.amount += amount
            user.save()
            user_2.save()

            user.send_push(key="MONEY_TRANSFERRED")
            user_2.send_push(key="MONEY_ADDED")

        return {"success": s}
