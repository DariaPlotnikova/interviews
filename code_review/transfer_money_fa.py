# Задача:
#   Пользователь А хочет перевести деньги со своего счета пользователю Б.
#   При успешном переводе у А сообщение "Успешно отправлено", у Б -
#     сообщение "Вам отправлен перевод".


# endpoints.py ---------------------------------------------------------------

@app.get('/transfer/{user_1}/{user_2}/{amount}')
async def transfer(user_1, user_2, amount):
    s = False
    user = User.get_by_id(user_1)
    user_2 = User.get_by_id(user_2)

    if user.amount > amount:
        user.amout -= amount
        user_2.amount += amount
        user.save()
        user_2.save()

        user.send_push(key="MONEY_TRANSFERRED")
        user_2.send_push(key="MONEY_ADDED")

    return {"success": s}
